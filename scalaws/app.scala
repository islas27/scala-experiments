import io.peregrine._
import scala.collection.mutable

case class Message(message: String)
case class User(name: String, age: Int)

object MyApp extends PeregrineApp {

  val db = mutable.Map[Int, User]()
  var index = 0

  //Save new User
  post("/api/users"){ req =>
    buildUser(req) match{
      case None => json(Message("No pude construir usuario."))
      case Some(u) => db.put(index, u)
        index = index+1
        json(Message("Creado")).status(201)
    }
  }

  //Get Users
  get("/api/users") { req =>
    json(db.values.toList)
  }

  //Get specified User
  get("/api/users/:id") { req =>
    val id = req.param("id").getOrElse("-1").toInt
    db.get(id) match {
      case None    => json(Message("NOT FOUND"))
      case Some(u) => json(u)
    }
  }

  //Update specified User
  put("/api/users/:id") { req =>
    val id = req.param("id").getOrElse("-1").toInt
    db.get(id) match {
      case None     => json(Message("NOT FOUND"))
      case Some(u)  =>
        buildUser(req) match {
          case None     => json(Message("Usuario no construido"))
          case Some(u)  => db.put(id, u)
            json(Message("Listo"))
        }
    }
  }

  //Delete specified User
  delete("/api/users/:id") { req =>
    val id = req.param("id").getOrElse("-1").toInt
    db.remove(id)
    json(Message("se fue"))
  }

  //Automation of building a User from a Request, or None
  def buildUser(req: Request): Option[User] = {
    for {
      name <- req.param("name")
      age  <- req.param("age")
    } yield User(name, age.toInt)
  }
}
